import Footer from './components/Footer';
import Header from './components/Header';
import Todos from './components/Todos';
import { useState } from 'react';
import './App.css';

function App() {
	const [todos, setTodos] = useState([
		{
			id: 1,
			title: 'Buy groceries',
			checked: false,
		},
		{
			id: 2,
			title: 'Finish homework',
			checked: true,
		},
		{
			id: 3,
			title: 'Go to the gym',
			checked: false,
		},
	]);

	const handleCheck = (id) => {
		const newTodos = todos.map((todo) => {
			if (todo.id === id) {
				todo.checked = !todo.checked;
			}
			return todo;
		});
		setTodos(newTodos);
		localStorage.setItem('todos', JSON.stringify(newTodos));
	};

	const onDelete = (id) => {
		const newTodos = todos.filter((todo) => todo.id !== id);
		setTodos(newTodos);
		localStorage.setItem('todos', JSON.stringify(newTodos));
	};

	return (
		<div className='app'>
			<Header title='Todo List' />
			<main>
				<Todos todos={todos} onDelete={onDelete} handleCheck={handleCheck} />
			</main>
			<Footer todos={todos} />
		</div>
	);
}

export default App;
