import { FaRegTrashCan } from 'react-icons/fa6';
import PropTypes from 'prop-types';

function TodoItem({ todo, onDelete, handleCheck }) {
	return (
		<li className='item'>
			<input
				type='checkbox'
				checked={todo.checked}
				value={todo}
				onChange={() => handleCheck(todo.id)}
			/>
			<label
				style={todo.checked ? { textDecoration: 'line-through' } : {}}
				onDoubleClick={() => handleCheck(todo.id)}>
				{todo.title}
			</label>
			<FaRegTrashCan
				role='button'
				tabIndex='0'
				onClick={() => onDelete(todo.id)}
			/>
		</li>
	);
}

TodoItem.propTypes = {
	todos: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number,
			text: PropTypes.string,
			completed: PropTypes.bool,
		})
	),
	todo: PropTypes.string,
	handleCheck: PropTypes.func,
	onDelete: PropTypes.func,
};

export default TodoItem;
