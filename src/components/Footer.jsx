import PropTypes from 'prop-types';

function Footer({ todos }) {
	return (
		<footer>
			<p>
				You have {todos.length} {todos.length === 1 ? 'todo' : 'todos'} in your
				list
			</p>
		</footer>
	);
}

Footer.propTypes = {
	todos: PropTypes.arrayOf(),
};

export default Footer;
