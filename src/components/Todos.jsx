import PropTypes from 'prop-types';
import TodoList from './TodoList';

function Todos({ todos, handleCheck, onDelete }) {
	return (
		<>
			{todos.length ? (
				<TodoList todos={todos} handleCheck={handleCheck} onDelete={onDelete} />
			) : (
				<p style={{ marginTop: '2rem' }}>No todos</p>
			)}
		</>
	);
}

Todos.propTypes = {
	todos: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number,
			text: PropTypes.string,
			completed: PropTypes.bool,
		})
	),
	handleCheck: PropTypes.func,
	onDelete: PropTypes.func,
};

export default Todos;
