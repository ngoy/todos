import PropTypes from 'prop-types';
import TodoItem from './TodoItem';

function TodoList({ todos, handleCheck, onDelete }) {
	return (
		<ul>
			{todos.map((todo) => (
				<TodoItem
					key={todo.id}
					todo={todo}
					handleCheck={handleCheck}
					onDelete={onDelete}
				/>
			))}
		</ul>
	);
}

TodoList.propTypes = {
	todos: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number,
			text: PropTypes.string,
			completed: PropTypes.bool,
		})
	),
	handleCheck: PropTypes.func,
	onDelete: PropTypes.func,
};

export default TodoList;
